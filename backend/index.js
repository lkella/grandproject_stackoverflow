// importing constants
const { PORT, URL } = require("./dependencies/constants/index");

// external dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");

// routing for user path
const user_route = require("./dependencies/routes/user");

// routing for question path
const question_route = require("./dependencies/routes/question");

// routing for answer path
const answer_route = require("./dependencies/routes/answer");

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(user_route);
app.use(question_route);
app.use(answer_route);

mongoose.connect(URL, { useNewUrlParser: true }, () => {
  // connected to mongoose, ONLY THEN we listen to PORT
  app.listen(PORT, () => {
    console.log(`Server started running on ${PORT}!`);
  });
});
