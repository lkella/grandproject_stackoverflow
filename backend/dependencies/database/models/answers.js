const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const model = mongoose.Schema({
  questionid: {
    type: Schema.Types.ObjectId,
    ref: "Questions",
  },
  Author: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
  Content: {
    type: String,
  },
  upvote: {
    type: String,
    default: [],
  },
  downvote: {
    type: String,
    default: [],
  },
});

module.exports = mongoose.model("Answers", model);
