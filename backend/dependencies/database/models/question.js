const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const model = mongoose.Schema({
  Title: {
    type: String,
    require: true,
  },
  Description: {
    type: String,
    require: true,
  },
  Author: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
  Timestamp: {
    type: String,
    require: true,
  },
  upvote: {
    type: String,
    default: [],
  },
  downvote: {
    type: String,
    default: [],
  },
  Replies: {
    type: Schema.Types.ObjectId,
    ref: "Answers",
  },
  Tags: {
    type: Array,
    default: [],
  },
  Views: {
    type: Number,
    default: 0,
  },
});

module.exports = mongoose.model("Questions", model);
