const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  email: {
    type: String,
    required: true,
    unique: true,
    match: [
      /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
      "Please provide a valid e-mail",
    ],
  },
  password: {
    type: String,
    required: true,
    match: [
      /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/,
      "provide valid password",
    ],
  },
  userQuestions: [
    {
      type: mongoose.Schema.ObjectId,
      ref: "Questions",
    },
  ],
  userAnswers: [
    {
      type: mongoose.Schema.ObjectId,
      ref: "Answers",
    },
  ],
});

const User = mongoose.model("User", userSchema);
module.exports = User;
