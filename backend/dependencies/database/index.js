const User = require("./models/user");
const Questions = require("./models/question");
const Answers = require("./models/answers");

module.exports = { User, Questions, Answers };
