//External imports
const e = require("express");
const express = require("express");
const router = express.Router();

// Internal database dependencies
const Questions = require("../database/models/question");
// const User = require("../database/models/user");
const Answers = require("../database/models/answers");

router.post("/addanswer", (req, res) => {
  const { questionid, Content } = req.body;
  // const Author = User.id;
  const answer = Answers({
    questionid,
    Content,
  });
  Questions.find({ questionid }, (err, data) => {
    if (err) {
      res.status(500).send({ err });
    } else {
      if (!questionid) {
        res.status(401).send({ msg: "No question found" });
      } else {
        answer.save((err, data) => {
          if (err) {
            res.status(500).send({ err });
          } else {
            res.status(200).send({ data });
          }
        });
      }
    }
  });
});

router.delete("/deleteanswer", (req, res) => {
  const { answerid } = req.body;
  Answers.findByIdAndRemove(answerid, (err) => {
    if (err) {
      res.status(500).send({ err: "", msg: "Internal Server Error" });
    } else if (!answerid) {
      res.status(407).send({ msg: "Not found any Answer with that id" });
    } else {
      res.status(202).send({ msg: "Answer deleted successfully" });
    }
  });
});

module.exports = router;
