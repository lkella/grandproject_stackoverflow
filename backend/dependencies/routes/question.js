//External imports
const e = require("express");
const express = require("express");
const router = express.Router();

// Internal database dependencies
const Questions = require("../database/models/question");
const User = require("../database/models/user");
const { route } = require("./user");

router.post("/addquestion", (req, res) => {
  const { Title, Description, Tags } = req.body;
  // const Author = User.id;

  const question = Questions({
    Title,
    Description,
    Tags,
  });
  question.save((err) => {
    if (err) {
      res.status(500).send({ err: "", msg: "Internal Server Error" });
    } else {
      res.status(201).send({ msg: "Question created successfully" });
    }
  });
});

router.delete("/deletequestion", (req, res) => {
  const { questionid } = req.body;
  Questions.findByIdAndRemove(questionid, (err) => {
    if (err) {
      res.status(500).send({ err: "", msg: "Internal Server Error" });
    } else if (!questionid) {
      res.status(407).send({ msg: "Not found any question with that id" });
    } else {
      res.status(202).send({ msg: "Question deleted successfully" });
    }
  });
});

router.put("/updatequestion", (req, res) => {
  const { questionid, Title, Description, Tags } = req.body;
  // const Author = User.id;

  const question = Questions({
    Title,
    Description,
    Tags,
  });
  Questions.findOneAndUpdate(
    { questionid: req.body.existingid },
    question,
    (err, data) => {
      if (err) {
        res.status(500).send({ err });
      } else {
        if (!questionid) {
          res.status(402).send({ msg: "No question Found" });
        } else {
          question.save();
          res.status(200).send({ data, msg: "updated  successfully" });
        }
      }
    }
  );
  // question.save((err) => {
  //   if (err) {
  //     res.status(500).send({ err: "", msg: "Internal Server Error" });
  //   } else {
  //     res.status(201).send({ msg: "Question created successfully" });
  //   }
  // });
});

router.post("/votes", (req, res) => {
  const { questionid, status } = req.body;
  Questions.findByIdAndUpdate(questionid, (err, data) => {
    if (err) {
      res.status(500).send({ msg: "Internal server error" });
    } else {
      if (!questionid) {
        res.status(402).send({ msg: "Question not found" });
      } else {
        if (req.body.status == 1) {
          Questions.upvote = Questions.upvote + 1;
        } else if (req.body.status == 0) {
          Questions.downvote = Questions.downvote + 1;
        }
        Questions.save();
        res.status(200).send({
          success: true,
          data,
          message: "Successfully Voted Question",
        });
      }
    }
  });
});

module.exports = router;
